import torch
import numpy as np
import cv2
import sys
import os
from attacks.attack_gen import attack_generator
from quantizers.quantizer import Quantizer
from data_utils.data_utils import  AdversarialDataset,AttackParser, load_model, model_image_size
import argparse
import foolbox as fb

parser = AttackParser(argparse.ArgumentParser())

for model_cpt,model_name in enumerate(parser.models):
    images = 0
    correctly_predicted = 0
    model, device = load_model(parser, model_cpt)

    img_size_var=model_image_size(model_name)
    print("img size: ", img_size_var)
    orig_set = AdversarialDataset(parser, img_size=img_size_var)

    orig_loader = torch.utils.data.DataLoader(orig_set, batch_size=parser.batch_size, shuffle=False)

    print(len(orig_loader), " batches")
    for i, data_batch in enumerate(orig_loader, 0):
        orig_batch, initial_label, image_nbs = data_batch

        if (i%50)==0:
            print(i*orig_batch.shape[0])

        model.zero_grad()
        model.eval()
        prediction = model(orig_batch)
        pred_label = torch.argmax(prediction,axis=-1)

        correctly_predicted+=(pred_label==initial_label).float().sum()
        images += orig_batch.shape[0]

    print("{}% accuracy on {} images. Model: {}".format(correctly_predicted/images*100, images, model_name))
