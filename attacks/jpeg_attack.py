from typing import Optional
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
from scipy.fftpack import dct, idct
from quantizers.color_coding import rgb_to_ycbcr,ycbcr_to_rgb,rgb_to_ycbcr_grad,ycbcr_to_rgb_grad
from quantizers.quant_tables import quantization_tables
from PIL import Image

class JPEG:
        def __init__(self,
                     num_classes: int,
                     device: torch.device = torch.device('cpu')):
            self.device = device
            self.num_classes = num_classes

        def __call__(self, model: nn.Module, inputs: torch.Tensor, criterion, image_nb) -> torch.Tensor:
            """
            Performs JPEG compression with growing quality factors.
            Saves the compressed image that is adversarial with the highest quality factor.
            """
            labels = criterion.labels.raw
            batch_size = inputs.shape[0]
            orig = inputs.to(self.device)

            quant_tables = quantization_tables(100)
            jpeg_quantized = spatial_to_jpeg(orig, quant_tables)
            best_adv = jpeg_quantized
            best_qf = torch.ones(batch_size, device=self.device)*100
            ever_adv = torch.zeros(batch_size, device=self.device)
            for i in range(0,110,10):
                quant_tables = quantization_tables(i)
                jpeg_quantized = spatial_to_jpeg(orig, quant_tables)
                png_quantized = jpeg_to_spatial(jpeg_quantized, quant_tables)
                predicted_labels = model(png_quantized).argmax(1)
                adversarial = (predicted_labels!=labels)
                best_adv = adversarial.view(batch_size,1,1,1)*jpeg_quantized+(~adversarial.view(batch_size,1,1,1))*best_adv
                best_qf = adversarial*i+(~adversarial)*best_qf
                ever_adv = ever_adv+adversarial

            for batch_image in range(batch_size):
                if ever_adv[batch_image]!=0:
                    quantized_images = png_quantized[batch_image]#[:, :, [2,1,0]]
                    im = Image.fromarray(quantized_images.cpu().numpy().astype(np.uint8))
                    im.save("outputs/jpeg_attack/{}.jpg".format(image_nb[0][batch_image]), quality=best_qf[batch_image].int().item(), subsampling=0)
                    np.save("outputs/jpeg_attack/{}.npy".format(image_nb[0][batch_image]),np.float32(best_adv[batch_image].cpu()) )

            return best_adv


def dct2(a):
    return dct(dct(a, axis=0, norm='ortho' ), axis=1, norm='ortho')

def idct2(a):
    return idct(idct(a, axis=0 , norm='ortho'), axis=1 , norm='ortho')

def dct2_8_8(image, quant_tables, quantization=True):
    imsize = image.shape
    dct = np.zeros(imsize)
    for batch_nb in range(image.shape[0]):
        for channel in range(imsize[3]):
            for i in np.r_[:imsize[1]:8]:
                for j in np.r_[:imsize[2]:8]:
                    dct[batch_nb, i:(i+8),j:(j+8), channel] = dct2(image[batch_nb, i:(i+8),j:(j+8), channel])
                    if quantization:
                        dct[batch_nb, i:(i+8),j:(j+8), channel] =  dct[batch_nb, i:(i+8),j:(j+8), channel]/quant_tables[:,:,channel]
    return dct

def idct2_8_8(dct, quant_tables, quantization=True):
    im_dct = np.zeros(dct.shape)
    for batch_im in range(dct.shape[0]):
        for channel in range(dct.shape[3]):
            for i in np.r_[:dct.shape[1]:8]:
                for j in np.r_[:dct.shape[2]:8]:
                    if quantization:
                        im_dct[batch_im, i:(i+8),j:(j+8), channel] = dct[batch_im, i:(i+8),j:(j+8), channel]*quant_tables[:,:,channel]
                    else:
                        im_dct[batch_im, i:(i+8),j:(j+8), channel] = dct[batch_im, i:(i+8),j:(j+8), channel]
                    im_dct[batch_im, i:(i+8),j:(j+8), channel] = idct2(im_dct[batch_im, i:(i+8),j:(j+8), channel] )
    return im_dct

def jpeg_to_spatial(jpeg_tensor, quant_tables, quantization=True):
    jpeg_array = jpeg_tensor.detach().cpu().numpy()
    jfif_tensor = idct2_8_8(jpeg_array, quant_tables,True)
    jfif_tensor = jfif_tensor+128
    spatial_tensor = ycbcr_to_rgb(torch.tensor(jfif_tensor))

    if quantization:
        spatial_tensor = spatial_tensor.round()
        spatial_tensor = torch.clamp(spatial_tensor,0,255)
    spatial_tensor = spatial_tensor.to(jpeg_tensor.device)
    return(spatial_tensor.float())

def spatial_to_jpeg(spatial_tensor, quant_tables, quantization=True):
    jfif_tensor = rgb_to_ycbcr(spatial_tensor)
    jfif_tensor = jfif_tensor.clamp(0,255)
    jfif_tensor = jfif_tensor-128
    jpeg_array = dct2_8_8(jfif_tensor.numpy(), quant_tables)

    if quantization:
        jpeg_array = np.around(jpeg_array)
    jpeg_tensor = torch.tensor(jpeg_array, device=spatial_tensor.device)
    return(jpeg_tensor.float())
