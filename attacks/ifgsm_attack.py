from typing import Optional
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np

class IFGSM:
    """
    FGSM attack: basic Fast Gradient Sign Method to perform a fast attack based on gradient sign.

    epsilon : if binary search is not performed: factor by which the gradient sign will be added to the inputs
    binary_search : if True, attack will be optimized on binary_search_steps steps between O and max_epsilon

    """

    def __init__(self,
                 num_classes: int,
                 epsilon: float = 4,
                 binary_search: bool = True,
                 binary_search_steps: int = 10,
                 steps: int = 10,
                 device: torch.device = torch.device('cpu')):


        self.epsilon = epsilon
        self.device = device
        self.b_search = binary_search
        self.b_search_steps = binary_search_steps
        self.num_classes = num_classes
        self.iterations = steps

    def __call__(self, model: nn.Module, inputs: torch.Tensor, criterion,
               targeted: bool = False, epsilons=None) -> torch.Tensor:
        """
        Performs the attack of the model for the inputs and labels.

        Parameters
        ----------
        model : nn.Module
            Model to attack.
        inputs : torch.Tensor
            Batch of samples to attack. Values should be in the [0, 1] range.
        labels : torch.Tensor
            Labels of the samples to attack if untargeted, else labels of targets.
        targeted : bool, optional
            Whether to perform a targeted attack or not.

        Returns
        -------
        torch.Tensor
            Batch of samples modified to be adversarial to the model.

        """
        labels = criterion.labels.raw
        batch_size = inputs.shape[0]
        multiplier = 1 if targeted else -1
        orig = inputs.to(self.device)
        lowers = torch.zeros(batch_size,1,1,1).to(self.device)
        uppers = torch.ones(batch_size,1,1,1).to(self.device)*self.epsilon

        prediction = model(inputs)
        pred_labels = torch.argmax(prediction,axis=-1)
        misclassified = (pred_labels!=labels).float().view(batch_size,1,1,1)

        best_l2 = 1e6*(1-misclassified)+misclassified*lowers
        best_adversarial_samples = orig

        for binary_step in range(self.b_search_steps):
            epsilons = (uppers+lowers)/2
            adversarial_samples = orig.detach().clone().to(self.device)
            adversarial_found = torch.zeros(batch_size,1,1,1, device=self.device)
            distortions = best_l2

            for i in range(self.iterations):
                adv_variable = torch.autograd.Variable(adversarial_samples,requires_grad=True).to(self.device)
                prediction = model(adv_variable)
                pred_labels = prediction.argmax(1)
                adversarial = (labels!=pred_labels).view(batch_size,1,1,1).float().to(self.device)
                adversarial_found = adversarial_found + adversarial

                labels_onehot = torch.zeros(labels.size(0), self.num_classes, device=self.device)
                labels_onehot.scatter_(1, labels.unsqueeze(1).long(),1)

                adversarial_loss = (prediction*labels_onehot).sum(axis=1)
                adversarial_loss.backward(torch.ones(batch_size, device=self.device))
                gradients = adv_variable.grad.data

                #Check if current sample is both adversarial and has a lower distortion
                adv_lower_dist = adversarial*(distortions<=best_l2).float()
                best_l2 = adv_lower_dist*distortions+best_l2*(1-adv_lower_dist)
                best_adversarial_samples = adv_lower_dist*adversarial_samples+best_adversarial_samples*(1-adv_lower_dist)

                #Update current adv
                adv_samples_temp = adversarial_samples+multiplier*epsilons.view(batch_size,1,1,1)*gradients.sign()
                adversarial_temp = torch.clamp(adv_samples_temp,0,255)

                #Project adversarial samples back on the ball
                perturbations = (adv_samples_temp - orig)
                distortions = perturbations.view(batch_size,-1).norm(dim=1).view(batch_size,1,1,1)
                adversarial_samples = adv_samples_temp

            found_in_iteration = (adversarial_found!=0).float()
            lowers = (1-found_in_iteration)*epsilons+found_in_iteration*lowers.to(self.device)
            uppers = found_in_iteration*epsilons+(1-found_in_iteration)*uppers.to(self.device)
        adversarial_samples = best_adversarial_samples.detach()*(1-misclassified)+inputs*misclassified
        return adversarial_samples, None, None
