from .fgsm_attack import FGSM
from .ifgsm_attack import IFGSM
from .pgd_attack import PGD
from .bp_attack import BP
from .jpeg_attack import JPEG
import sys

def attack_generator(arg_parser, attack_cpt, epsilons=1):
    attack_name, pytorch_device = arg_parser.attacks[attack_cpt], arg_parser.device
    print('running {}'.format(attack_name))
    if attack_name == 'FGSM':
        return(FGSM(epsilon=arg_parser.epsilon, num_classes=1000, device=pytorch_device, binary_search=True))
    if attack_name == 'IFGSM':
        return(IFGSM(epsilon=arg_parser.epsilon, num_classes=1000, device=pytorch_device, binary_search=True))
    elif attack_name == 'PGD':
        return(PGD(max_epsilon=arg_parser.epsilon, num_classes=1000, device=pytorch_device, binary_search=True))
    elif attack_name == 'BP':
        return(BP(num_classes=1000, device=pytorch_device))
    elif attack_name == 'JPEG':
        return(JPEG(num_classes=1000, device=pytorch_device))
