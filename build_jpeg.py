import numpy as np
import jpegio
import glob
import os
import argparse
import time

parser_obj = argparse.ArgumentParser()

parser_obj.add_argument('--inputs', type=str, default='outputs/measures', help='path to folder containing measures')
parser_obj = parser_obj.parse_args()
folder = parser_obj.inputs

if not os.path.isdir(folder+'/built'):
    os.makedirs(folder+'/built')

while True:  #Let it build JPEG images as they are produced by quantization
    files = glob.glob(folder+'/*g')

    for file in files:
        if cpt%100==0:
            print(cpt)
        source_jpeg = jpegio.read(file)
        source_jpeg.optimize_coding = True

        target = np.load(file[:-4]+'.npy')
        for i in range(3):
        	source_jpeg.coef_arrays[i][:,:] = target[:,:,i]

        source_jpeg.write(folder+'/built/'+file[-9:])
        os.remove(file)
        os.remove(file[:-4]+'.npy')
        cpt+=1
    time.sleep(20)
