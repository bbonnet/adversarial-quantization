from .dct_quantiz import best_quantization_dct
from .l2_quantiz import best_quantization_spatial
import torch
import numpy as np

class Quantizer:
    def __init__(self, pytorch_model, arg_parser , number_classes=1000, binary_search_steps=10):
        self.quantizer = arg_parser.quantization_method
        self.max_dist = arg_parser.max_deg
        self.pytorch_device = arg_parser.device
        self.model = pytorch_model
        self.num_classes = 1000
        self.jpeg_quant =  arg_parser.jpeg_quality
        self.binary_search_steps = binary_search_steps

    def classif_loss(self, init_label, adv_label, py_image, real=False):
        """
        The loss used to study adverariality. If <0, then the image is adversarial.
        real boolean controls wether the second term is fixed (decided by adv_label) or the second argmax.
        """
        prediction = self.model(py_image)
        init_labels_onehot = torch.zeros(init_label.size(0), 1000, device=self.pytorch_device)
        init_labels_onehot.scatter_(1, init_label.unsqueeze(1).long(),1)

        #Compute classification with the same adversarial label even if it is not the second argmax anymore
        if not real:
            adv_labels_onehot = torch.zeros(adv_label.size(0), 1000, device=self.pytorch_device)
            adv_labels_onehot.scatter_(1, adv_label.unsqueeze(1).long(),1)

            classification_loss = prediction*init_labels_onehot-prediction*adv_labels_onehot
            classification_loss = classification_loss.sum(axis=1)

        #Take the second argmax which may not remain the same throughout the experiment
        if real:
            labels = prediction.argsort(1)
            adv_class = labels[:,-1]
            adv_class = (adv_class==init_label)*labels[:,-2]+(adv_class!=init_label)*adv_class

            adv_labels_onehot = torch.zeros(adv_class.size(0), 1000, device=self.pytorch_device)
            adv_labels_onehot.scatter_(1, adv_class.unsqueeze(1).long(),1)

            classification_loss = prediction*init_labels_onehot-prediction*adv_labels_onehot
            classification_loss = classification_loss.sum(axis=1)

        return(classification_loss)

    def find_grads(self, initial_labels, adversarial_images, real_loss=False):
        self.model.zero_grad()
        adversarial_variables = torch.autograd.Variable(adversarial_images, requires_grad=True)
        prediction_adv = self.model(adversarial_variables)
        adversarial_labels  = prediction_adv.argsort(1)[:,-2]
        really_adv = (adversarial_labels!=initial_labels)
        adversarial_labels = really_adv*adversarial_labels + (~really_adv)*prediction_adv.argsort(1)[:,-1]

        loss = self.classif_loss(initial_labels, adversarial_labels, adversarial_variables, real_loss)
        loss.sum().backward()
        gradients = adversarial_variables.grad.data

        return(gradients, adversarial_labels)

    def quantize_samples(self, adv_element, orig_element, label_element):
        if self.quantizer == 'L2':
            quantized_adv = best_quantization_variance(self, adv_element, orig_element, label_element)
        elif self.quantizer == 'JPEG':
            quantized_adv= best_quantization_dct(self, adv_element, orig_element, label_element)
        elif self.quantizer == 'ROUND':
            quantized_adv = adv_element.round()
        return(quantized_adv)
