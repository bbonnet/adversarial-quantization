import torch
import numpy as np


def rgb_to_ycbcr(bgr_tensor):
    r, g, b = bgr_tensor[:,:,:,0], bgr_tensor[:,:,:,1], bgr_tensor[:,:,:,2]
    ycbcr_tensor = torch.zeros(bgr_tensor.shape).double()
    ycbcr_tensor[:,:,:,0] = 0.299*r + 0.587*g + 0.114*b
    ycbcr_tensor[:,:,:,1] = 128- 0.16874*r-0.33126*g+0.5*b
    ycbcr_tensor[:,:,:,2] = 128+0.5*r-0.41869*g-0.08131*b
    return(ycbcr_tensor)

def ycbcr_to_rgb(ycbcr_tensor):
    Y, Cb, Cr = ycbcr_tensor[:,:,:,0],ycbcr_tensor[:,:,:,1],ycbcr_tensor[:,:,:,2]
    bgr_tensor = torch.zeros(ycbcr_tensor.shape)
    bgr_tensor[:,:,:,0] = Y + 1.40200 * (Cr-128)
    bgr_tensor[:,:,:,1] = Y - 0.34414 * (Cb-128) - 0.71414 *(Cr-128)
    bgr_tensor[:,:,:,2] = Y + 1.77200 * (Cb-128)
    return(bgr_tensor)

def rgb_to_ycbcr_grad(bgr_tensor):
    r, g, b = bgr_tensor[:,:,:,0], bgr_tensor[:,:,:,1], bgr_tensor[:,:,:,2]
    ycbcr_tensor = torch.zeros(bgr_tensor.shape).double()
    ycbcr_tensor[:,:,:,0] = 0.299*r + 0.587*g + 0.114*b
    ycbcr_tensor[:,:,:,1] = -0.16874*r-0.33126*g+0.5*b
    ycbcr_tensor[:,:,:,2] = 0.5*r-0.41869*g-0.08131*b

    return(ycbcr_tensor)

def ycbcr_to_rgb_grad(ycbcr_tensor):
    Y, Cb, Cr = ycbcr_tensor[:,:,:,0],ycbcr_tensor[:,:,:,1],ycbcr_tensor[:,:,:,2]
    bgr_tensor = torch.zeros(ycbcr_tensor.shape)
    bgr_tensor[:,:,:,0] = Y + 1.40200 * Cr
    bgr_tensor[:,:,:,1] = Y - 0.34414 * Cb - 0.71414 * Cr
    bgr_tensor[:,:,:,2] = Y + 1.77200 * Cb
    return(bgr_tensor)
