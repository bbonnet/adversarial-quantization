import torch
import numpy as np

def best_quantization_spatial(quantizer, adversarial_images, images_orig, init_labels):
    """
    This is the main function for the  steganographic quantization with the quadratic method.
    adversarial_images = Tensor of the already attacked images
    images_orig = Tensor of the original images before attack
    init_labels = Ground Truth labels for images
    """
    batch_size, height, width, channels = adversarial_images.shape
    perturbations_adv = adversarial_images - images_orig

    grads, adv_labels = quantizer.find_grads(init_labels, adversarial_images)
    loss = quantizer.classif_loss(init_labels, adv_labels, images_orig)
    already_adv = (loss<=0).float()

    loss_th_base =  quantizer.classif_loss(init_labels, adv_labels, adversarial_images)
    perturbation_term = (grads*(adversarial_images-images_orig)).view(batch_size,-1).sum(axis=1)
    lambdas_critic = find_lambdas_critic(grads, loss_th_base-perturbation_term)

    quantization_mat = quantize_lambda(quantizer, perturbations_adv, grads, 100*lambdas_critic)
    adversarial = torch.clamp(quantization_mat+images_orig, 0, 255)
    best_adversarial = adversarial

    lower_lambda = torch.zeros(batch_size, device=quantizer.pytorch_device)
    upper_lambda = 100*lambdas_critic
    lambda_search = (upper_lambda+lower_lambda)/2

    for binary_search_step in range(quantizer.binary_search_steps):
        quantization_mat = quantize_lambda(quantizer, perturbations_adv, grads, lambda_search)
        adversarial = torch.clamp(quantization_mat+images_orig, 0, 255)
        loss = quantizer.classif_loss(init_labels, adv_labels, adversarial)
        distortion = (quantization_mat+perturbations_adv).norm().item()

        best_adversarial = (loss<=0).float().view(batch_size,1,1,1)*adversarial + (loss>0).float().view(batch_size,1,1,1)*best_adversarial

        lower_lambda = (loss>0).float()*lambda_search + (loss<=0).float()*lower_lambda
        upper_lambda = (loss<=0).float()*lambda_search + (loss>0).float()*upper_lambda
        lambda_search = (lower_lambda+upper_lambda)/2
    best_adversarial = already_adv.view(batch_size,1,1,1)*images_orig + (1-already_adv).view(batch_size,1,1,1)*best_adversarial
    return(best_adversarial.detach())


def find_lambdas_critic(gradients, loss_th_0):
    """
    This function calculates all the possibles values of Lambda for which the quantization of a pixel
    will swap from minimum distortion to maximum distortion
    """
    batch_size = gradients.shape[0]
    grad_norm_squared = (gradients.view(batch_size,-1).norm(dim=1))**2

    lambda_critic = 2*(loss_th_0)/grad_norm_squared
    lambda_critic = torch.max(lambda_critic, torch.ones(lambda_critic.shape, device=lambda_critic.device))

    return(lambda_critic)

def quantize_lambda(quantizer, perturbations, gradients, lambda_val):
    """
    This function returns the quantized perturbation for a given value of lambda.
    """
    maximum_distortion = quantizer.max_dist
    batch_size = perturbations.shape[0]

    #Applying the quantization method and keeping it inside the maximum distortion boundaries
    quantized = -lambda_val.view(batch_size,1,1,1)*gradients/2
    quantized = quantized.round().to(quantizer.pytorch_device)
    #quantized_bounded = torch.clamp(quantized,-maximum_distortion, maximum_distortion)

    if quantizer.max_dist%2==0:
        max_pert = (perturbations.round() + quantizer.max_dist/2)
        min_pert = (perturbations.round() - quantizer.max_dist/2)
    else:
        max_pert = (perturbations.ceil() + (quantizer.max_dist-1)/2)
        min_pert = (perturbations.floor() - (quantizer.max_dist-1)/2)

    quantized_bounded_min = torch.min(max_pert.int().float(), quantized)
    quantized_bounded = torch.max(min_pert.int().float(), quantized_bounded_min)

    #quantized_bounded = torch.max(quantized,perturbations.floor() - maximum_distortion-1)
    #quantized_bounded = torch.min(quantized_bounded,perturbations.ceil() + maximum_distortion-1)
    #This ensures that the quantization doesn't go backward and cancels the perturbation
    quantized_bounded[perturbations>0] = torch.max((quantized_bounded[perturbations>0]), torch.zeros(perturbations[perturbations>0].shape).to(quantizer.pytorch_device))
    quantized_bounded[perturbations<0] = torch.min((quantized_bounded[perturbations<0]), torch.zeros(perturbations[perturbations<0].shape).to(quantizer.pytorch_device))
    return(quantized_bounded)
